resource "aws_route53_record" "interview" {
  zone_id = data.aws_route53_zone.zone.zone_id
  name    = "interview"
  type    = "CNAME"
  ttl     = "300"
  records = [kubernetes_service.live.status.0.load_balancer.0.ingress.0.hostname]
}

data "aws_route53_zone" "zone" {
  name         = var.dns_zone
}