resource "kubernetes_namespace" "live" {
  metadata {
    name = "live"
  }
}
resource "kubernetes_deployment" "live" {
  metadata {
    name      = "interview"
    namespace = kubernetes_namespace.live.metadata.0.name
  }
  spec {
    replicas = 2
    selector {
      match_labels = {
        app = "API"
      }
    }
    template {
      metadata {
        labels = {
          app = "API"
        }
      }
      spec {
        container {
          image = var.container_image
          name  = "interview-api"
          port {
            container_port = 5000
          }
          readiness_probe {
            http_get {
              path = "/healthz"
              port = 5000
            }
            initial_delay_seconds = 10
            period_seconds        = 5
          }
        } 
      }
    }
  }
}
resource "kubernetes_service" "live" {
  metadata {
    name      = "interview-api"
    namespace = kubernetes_namespace.live.metadata.0.name
  }
  spec {
    selector = {
      app = kubernetes_deployment.live.spec.0.template.0.metadata.0.labels.app
    }
    type = "LoadBalancer"
    port {
      protocol   = "TCP"
      port        = 80
      target_port = 5000
    }
  }
  depends_on = [module.eks]
}