ifneq (,$(wildcard ./.env))
	include .env
	export
endif

ifeq (output,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif

.ONESHELL:
SHELL := /bin/bash
.PHONY: apply destroy-backend destroy destroy-target plan-destroy plan plan-target prep apply-auto destroy-auto show validate output
VARS="variables/$(ENVIRONMENT)-$(DEPLOYMENT).tfvars"
CURRENT_FOLDER=$(shell basename "$$(pwd)")
ifeq ($(S3_BUCKET),)
	S3_BUCKET := "$(APPLICATION)-$(ENVIRONMENT)-$(DEPLOYMENT)-terraform"
endif
ifeq ($(DYNAMODB_TABLE),)
	DYNAMODB_TABLE := "$(APPLICATION)-$(ENVIRONMENT)-$(DEPLOYMENT)-terraform"
endif
WORKSPACE="$(ENVIRONMENT)-$(DEPLOYMENT)"
BOLD=$(shell tput bold)
RED=$(shell tput setaf 1)
GREEN=$(shell tput setaf 2)
YELLOW=$(shell tput setaf 3)
RESET=$(shell tput sgr0)

help:
	@grep '^[a-zA-Z]' "Makefile" | \
    sort | \
    awk -F ':.*?## ' 'NF==2 {printf "\033[36m  %-25s\033[0m %s\n", $$1, $$2}'

set-env:
	@if [ -z $(APPLICATION) ]; then \
		echo "$(BOLD)$(RED)APPLICATION was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(ENVIRONMENT) ]; then \
		echo "$(BOLD)$(RED)ENVIRONMENT was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(DEPLOYMENT) ]; then \
		echo "$(BOLD)$(RED)DEPLOYMENT was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ -z $(AWS_REGION) ]; then \
		echo "$(BOLD)$(RED)AWS_REGION was not set$(RESET)"; \
		ERROR=1; \
	 fi
	@if [ ! -z $${ERROR} ] && [ $${ERROR} -eq 1 ]; then \
		echo "$(BOLD)Example usage: \`APPLICATION=whatever DEPLOYMENT=demo ENVIRONMENT=staging REGION=us-east-2 make plan\`$(RESET)"; \
		exit 1; \
	 fi
	@if [ ! -f "$(VARS)" ]; then \
		echo "$(BOLD)$(RED)Could not find variables file: $(VARS)$(RESET)... creating now!"; \
		touch $(VARS); \
	 fi

prep: set-env  ## Prepare a new workspace (environment) if needed, configure the tfstate backend, update any modules, and switch to the workspace
	@echo "$(BOLD)Verifying that the S3 bucket $(S3_BUCKET) for remote state exists$(RESET)"
	@if ! aws s3api head-bucket --region $(AWS_REGION) --bucket $(S3_BUCKET) > /dev/null 2>&1 ; then \
		echo "$(BOLD)S3 bucket $(S3_BUCKET) was not found, creating new bucket with versioning enabled to store tfstate$(RESET)"; \
		aws s3api create-bucket \
			--bucket $(S3_BUCKET) \
			--acl private \
			--region $(AWS_REGION) > /dev/null 2>&1 ; \
		aws s3api put-bucket-encryption \
			--bucket $(S3_BUCKET) \
			--server-side-encryption-configuration '{"Rules": [{"ApplyServerSideEncryptionByDefault": {"SSEAlgorithm": "AES256"}}]}' > /dev/null 2>&1 ; \
		aws s3api put-bucket-versioning \
			--bucket $(S3_BUCKET) \
			--versioning-configuration Status=Enabled > /dev/null 2>&1 ; \
		echo "$(BOLD)$(GREEN)S3 bucket $(S3_BUCKET) created$(RESET)"; \
	 else \
		echo "$(BOLD)$(GREEN)S3 bucket $(S3_BUCKET) exists$(RESET)"; \
	 fi
	@echo "$(BOLD)Verifying that the DynamoDB table exists for remote state locking$(RESET)"
	@if ! aws dynamodb describe-table --table-name $(DYNAMODB_TABLE) > /dev/null 2>&1 ; then \
		echo "$(BOLD)DynamoDB table $(DYNAMODB_TABLE) was not found, creating new DynamoDB table to maintain locks$(RESET)"; \
		aws dynamodb create-table \
        	--region $(AWS_REGION) \
        	--table-name $(DYNAMODB_TABLE) \
        	--attribute-definitions AttributeName=LockID,AttributeType=S \
        	--key-schema AttributeName=LockID,KeyType=HASH \
        	--provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5 > /dev/null 2>&1 ; \
		echo "$(BOLD)$(GREEN)DynamoDB table $(DYNAMODB_TABLE) created$(RESET)"; \
		echo "Sleeping for 10 seconds to allow DynamoDB state to propagate through AWS"; \
		sleep 10; \
	 else \
		echo "$(BOLD)$(GREEN)DynamoDB Table $(DYNAMODB_TABLE) exists$(RESET)"; \
	 fi
	@echo "$(BOLD)Configuring the terraform backend$(RESET)"
	@terraform init \
		-input=false \
		-force-copy \
		-lock=true \
		-upgrade \
		-verify-plugins=true \
		-backend=true \
		-backend-config="region=$(AWS_REGION)" \
		-backend-config="bucket=$(S3_BUCKET)" \
		-backend-config="key=$(ENVIRONMENT)/$(CURRENT_FOLDER)/terraform.tfstate" \
		-backend-config="dynamodb_table=$(DYNAMODB_TABLE)"\
	    -backend-config="acl=private"
	@echo "$(BOLD)Switching to workspace $(WORKSPACE)$(RESET)"
	@terraform workspace select $(WORKSPACE) || terraform workspace new $(WORKSPACE)

plan: prep ## Show what terraform thinks it will do
	@terraform plan \
		-no-color \
		-lock=true \
		-input=false \
		-refresh=true \
		-var-file="$(VARS)" \
		-var 'environment=$(ENVIRONMENT)' \
		-var 'deployment=$(DEPLOYMENT)' \
		-var 'application=$(APPLICATION)' \
		-out=plan.tfplan

format: ## Rewrites all Terraform configuration files to a canonical format.
	@terraform fmt \
		-write=true \
    -recursive

# https://github.com/terraform-linters/tflint
lint: prep ## Check for possible errors, best practices, etc in current directory!
	@tflint

# https://github.com/liamg/tfsec
check-security: prep ## Static analysis of your terraform templates to spot potential security issues.
	@tfsec .

documentation: ## Generate README.md for a module
	@terraform-docs \
		. > README.md
		
plan-target: prep ## Shows what a plan looks like for applying a specific resource
	@echo "$(YELLOW)$(BOLD)[INFO]   $(RESET)"; echo "Example to type for the following question: module.rds.aws_route53_record.rds-master"
	@read -p "PLAN target: " DATA && \
		terraform plan \
			-no-color \
			-lock=true \
			-input=true \
			-refresh=true \
			-var-file="$(VARS)" \
			-var 'environment=$(ENVIRONMENT)' \
			-var 'deployment=$(DEPLOYMENT)' \
			-target=$$DATA

plan-destroy: prep ## Creates a destruction plan.
	@terraform plan \
		-no-color \
		-input=false \
		-refresh=true \
		-destroy \
		-var-file="$(VARS)" \
		-var 'environment=$(ENVIRONMENT)' \
		-var 'deployment=$(DEPLOYMENT)' \
		-var 'application=$(APPLICATION)' \

apply: prep ## Have terraform do the things. This will cost money.
	@terraform apply \
		-lock=true \
		-input=false \
		-refresh=true \
		-var-file="$(VARS)" \
		-var 'environment=$(ENVIRONMENT)' \
		-var 'deployment=$(DEPLOYMENT)' \
		-var 'application=$(APPLICATION)' \

apply-auto: prep ## Have terraform do the things automatically. This will cost money.
	@terraform apply \
	    -no-color \
	    -auto-approve \
		-lock=true \
		-input=false \
		-refresh=true \
		plan.tfplan

show: prep ## Show the current state
	@terraform show \
			-no-color \

validate: prep ## Show the current state
	@terraform validate \
			-no-color \

output: prep ## Show the current state outputs. This command takes arguments if you want to output a targetted output. 
	@terraform output $(RUN_ARGS)\
			-no-color \

destroy: prep ## Destroy the things
	@terraform destroy \
		-lock=true \
		-input=false \
		-refresh=true \
		-var-file="$(VARS)" \
		-var 'environment=$(ENVIRONMENT)' \
		-var 'deployment=$(DEPLOYMENT)' \
		-var 'application=$(APPLICATION)' \

destroy-auto: prep ## Destroy the things automatically
	@terraform destroy \
	    -no-color \
	    -auto-approve \
		-lock=true \
		-input=false \
		-refresh=true \
		-var-file="$(VARS)" \
		-var 'environment=$(ENVIRONMENT)' \
		-var 'deployment=$(DEPLOYMENT)' \
		-var 'application=$(APPLICATION)' \

destroy-target: prep ## Destroy a specific resource. Caution though, this destroys chained resources.
	@echo "$(YELLOW)$(BOLD)[INFO] Specifically destroy a piece of Terraform data.$(RESET)"; echo "Example to type for the following question: module.rds.aws_route53_record.rds-master"
	@read -p "Destroy target: " DATA && \
		terraform destroy \
		-lock=true \
		-input=false \
		-refresh=true \
		-var-file="$(VARS)" \
		-var 'environment=$(ENVIRONMENT)' \
		-var 'deployment=$(DEPLOYMENT)' \
		-var 'application=$(APPLICATION)' \
		-target=$$DATA

delete-confirm: 
	@echo "$(BOLD)$(RED)THIS WILL DESTROY TERRAFORM BACKEND AND STATE FOR $(ENVIRONMENT)-$(DEPLOYMENT)$(RESET)"; \
	    read -p '$(BOLD)Are you sure you want to continue? [y/Y]: $(RESET)' ANSWER && \
    	if [ "$${ANSWER}" == "y" ] || [ "$${ANSWER}" == "Y" ]; then \
			echo "$(BOLD)Attempting to delete...$(RESET)"; \
		else \
			echo "$(BOLD)Aborting...$(RESET)"; \
			exit 1; \
		fi

destroy-backend: delete-confirm ## Used to destroy the backend and state.
	@if ! aws dynamodb describe-table --region $(AWS_REGION) --table-name $(DYNAMODB_TABLE) > /dev/null 2>&1 ; then \
		echo "$(BOLD)$(RED)DynamoDB table $(DYNAMODB_TABLE) does not exist.$(RESET)"; \
	 else \
		if ! aws dynamodb delete-table --region $(AWS_REGION) --table-name $(DYNAMODB_TABLE) > /dev/null 2>&1 ; then \
			echo "$(BOLD)$(RED)Unable to delete DynamoDB table $(DYNAMODB_TABLE)$(RESET)"; \
		else \
			echo "$(BOLD)$(RED)DynamoDB table $(DYNAMODB_TABLE) does not exist.$(RESET)"; \
		fi \
	 fi
	@if ! aws s3api head-bucket --region $(AWS_REGION) --bucket $(S3_BUCKET) > /dev/null 2>&1 ; then \
		echo "$(BOLD)$(RED)S3 bucket $(S3_BUCKET) does not exist.$(RESET)"; \
	 else \
		if ! aws s3api delete-objects \
			--region $(AWS_REGION) \
			--bucket $(S3_BUCKET) \
			--delete "$$(aws s3api list-object-versions \
							--region $(AWS_REGION) \
							--bucket $(S3_BUCKET) \
							--output=json \
							--query='{Objects: Versions[].{Key:Key,VersionId:VersionId}}')" > /dev/null 2>&1 ; then \
				echo "$(BOLD)$(RED)Unable to delete objects in S3 bucket $(S3_BUCKET)$(RESET)"; \
		fi; \
		if ! aws s3api delete-objects \
			--region $(AWS_REGION) \
			--bucket $(S3_BUCKET) \
			--delete "$$(aws s3api list-object-versions \
							--region $(AWS_REGION) \
							--bucket $(S3_BUCKET) \
							--output=json \
							--query='{Objects: DeleteMarkers[].{Key:Key,VersionId:VersionId}}')" > /dev/null 2>&1 ; then \
				echo "$(BOLD)$(RED)Unable to delete markers in S3 bucket $(S3_BUCKET)$(RESET)"; \
		fi; \
		if ! aws s3api delete-bucket \
			--region $(AWS_REGION) \
			--bucket $(S3_BUCKET) > /dev/null 2>&1 ; then \
				echo "$(BOLD)$(RED)Unable to delete S3 bucket $(S3_BUCKET) itself$(RESET)"; \
		fi; \
	 fi	

