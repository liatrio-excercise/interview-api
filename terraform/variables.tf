variable "environment" {
    type = string
    description = "This is the environment for these resources"
}

variable "deployment" {
    type = string
    description = "This is the deployment being created"
}

variable "application" {
    type = string
    description = "This is the name of the application or product"
}

variable "container_image" {
    type = string
    description = "The container image which should be deployed."
    default = "registry.gitlab.com/liatrio-excercise/interview-api:latest"
}

variable "dns_zone" {
    type = string
    description = "The dns zone (domain name) to attach the application to."
    //default = "test.devopschris.com."
}