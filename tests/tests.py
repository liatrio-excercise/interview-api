import unittest
import src.api as api
import json
import sys
import time


class TestFlaskApi(unittest.TestCase):
    def setUp(self):
        self.app = api.app.test_client()

    def test_healthz(self):
        response = self.app.get('/healthz')
        self.assertEqual(
            json.loads(response.get_data().decode(sys.getdefaultencoding())),
            {"message": "ok"}
        )

    def test_api_message(self):
        response = self.app.get('/api')
        self.assertTrue(
            json.loads(response.get_data().decode(sys.getdefaultencoding()))["message"] == "Automate all the things!"
        )

    def test_api_timestamp(self):
        response = self.app.get('/api')
        self.assertTrue(
            json.loads(response.get_data().decode(sys.getdefaultencoding()))["timestamp"] in range(int(time.time()) -2, int(time.time()) +2)
        )

    def test_version(self):
        response = self.app.get('/version')
        self.assertEqual(
            json.loads(response.get_data().decode(sys.getdefaultencoding())),
            {"version": api.version}
        )

    def test_index(self):
        response = self.app.get('/')
        self.assertTrue(
            response.get_data().decode('utf8').startswith('<h1>')
        )



if __name__ == '__main__':
    unittest.main()
