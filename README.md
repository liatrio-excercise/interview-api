# Liatrio Interview API
[![pipeline status](https://gitlab.com/liatrio-excercise/interview-api/badges/master/pipeline.svg)](https://gitlab.com/liatrio-excercise/interview-api/commits/master)
[![coverage](https://gitlab.com/liatrio-excercise/interview-api/badges/master/coverage.svg)](https://gitlab.com/liatrio-excercise/interview-api/commits/master)

The Liatrio Interview API is a lightweight example REST API that returns a static message and timestamp.

Example output:
```
{
    "message":"Automate all the things!",
    "timestamp":1614836619
}
```
## Table of Contents

* [Example](##example)
* [API Endpoints](##api-endpoints)
   * [/api](###api) 
   * [/healthz](###health-check) 
   * [/version](###version) 
* [Local Development](##local-development)
  * [Python](###local-python-setup) 
   * [Testing](###testing) 
   * [Docker](###using-docker) 
* [Terraform](##Terraform)
* [Make](##The-Makefile)
* [Deployment](##Deployment)
* [Kubernetes](##Kubernetes)
* [Terraform Docs](##Terraform-Documentation)

## Example 

You can consume a hosted version of `Liatrio Interview API` at <a href="http://interview.live.devopschris.com/api" target="\_blank">devopschris.com</a>.

The API endpoint can be found at:   
http://interview.live.devopschris.com/api


## API Endpoints

Description of each of the API endpoints.

### API
----  
  Returns json message with timestamp.

* **URL**

  /api

* **Method:**

  `GET`
*  **URL Params**  

    None

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"message":"Automate all the things!","timestamp":1614836619}`

* **Sample Call:**

  ```
  curl -i http://interview.live.devopschris.com/api
  ```

### Health Check
----  
  Returns an ok message. (Used for health checks.)

* **URL**

  /healthz

* **Method:**

  `GET`
*  **URL Params**  

    None

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"message":"ok"}`

* **Sample Call:**

  ```bash
  curl -i http://interview.live.devopschris.com/healthz
  ```

### Version
----  
  Returns an ok message. (Used for health checks.)

* **URL**

  /version

* **Method:**

  `GET`
*  **URL Params**  

    None

* **Data Params**

    None

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"version":"x.x.x"}`

* **Sample Call:**

  ```bash
  curl -i http://interview.live.devopschris.com/version
  ```

## Local Development  
There are a couple of options for running local development. The API can be run in your own local Python environment, or in a container. Also development with AWS is possible from a local context with the proper AWS credentials.

### Requirements

* Python 3.x
* Git
* Terraform 0.14.x
* Make
* Bash
* AWS-CLIv2
* Docker
* Kubectl

### Local Python Setup

Clone the repo and install the dependencies.

```bash
git clone https://gitlab.com/liatrio-excercise/interview-api.git
cd interview-api
```

```bash
#(Optional) Configure your local environment first, then...
pip install -r requirements.txt
```

### Steps to run locally

To start the api server, run the following:

```bash
python3 -m src.api
```

Open [http://localhost:5000](http://localhost:5000) and try any endpoints listed above.

### Testing

Linting

```
pip install flake8
flake8
```

Unit Testing and Test Coverage

```bash
# Using Pytest
pip install pytest pytest-cov
python3 -m pytest --cov=src tests/tests.py
```

### Using Docker  
You can also run this app as a Docker container:

Step 1: Clone the repo

```bash
git clone https://gitlab.com/liatrio-excercise/interview-api.git
```

Step 2: Build the Docker image

```bash
docker build -t interview-api .
```

Step 3: Run the Docker container locally:

```bash
docker run -p 5000:5000 -d interview-api
```

Alternatively, you can also run the latest build this way:

```bash
docker login registry.gitlab.com
docker run -p 5000:5000 -d registry.gitlab.com/liatrio-excercise/interview-api:latest
```

Open [http://localhost:5000](http://localhost:5000) and try any endpoints listed above.

## Terraform  
The AWS Infrastructure and deployment is completed accomplished through Terraform. You must have access to the production deployment, or your own AWS account with credentials stored in your local environment to use the IaC. Terraform may be used traditionally, but a Makefile has been added to help with consistency across environments. 

[Terraform Docs Reference](##Terraform-Documentation)

## The Makefile  
A makefile is in place to simplify state management and execution of persistent and consistent infrastructure accross deployment environments.

```
make help
```

### IMPORTANT : Destroying Backend  
Since the Makefile will create a backend for you, if at any time you need to delete the backend, the target ```make destroy-backend``` will do the job. Just remember that all state will be removed!!

### Local Environment  
Configure your local environment with these variables.
```
APPLICATION=application # ex. interview
ENVIRONMENT=environment # ex. prod
DEPLOYMENT=deployment # ex. live
AWS_ACCESS_KEY_ID=<your_aws_access_key_id>
AWS_SECRET_ACCESS_KEY=u<your_aws_secret_access_key>
AWS_REGION=us-east-1
```
## Deployment

Most make targets will build an s3 backend, with lock in dynamodb (Format: \<APPLICATION>-\<ENVIRONMENT>-\<DEPLOYMENT>-terraform), and also generate an empty \<ENVIRONMENT>-\<DEPLOYMENT>.tfvars file in the variables directory if one does not exist.

To configure deployment
```
make prep
```

Modify the variables/\<ENVIRONMENT>-\<DEPLOYMENT>.tfvars, adding container\_image="registry.gitlab.com/liatrio-excercise/interview-api:latest" and 
dns_zone="your-domain.com." to the file.

* Note, you can also add TF\_VAR\_container\_image="registry.gitlab.com/liatrio-excercise/interview-api:latest" and/or TF\_VAR\_dns\_zone="your-domain.com." in your local environment to avoid that file configuration.

To generate a plan, prior to running. Use make plan.
```
make plan
```

To deploy, use make apply
```
make apply
```

The make apply will create the following AWS resources:  

* VPC 
  * New VPC
  * Internet Gateway
  * Subnets (Public and Private)
  * NAT Gateway
  * Route Tables
* EKS
  * EKS Cluster
  * IAM Roles
  * Security Groups
  * Worker Node Group
* Kubernetes 
  * Namespace
  * Service
  * Deployment
* Route53
  * Zone/Record 


To destroy, use make destroy (Note: This will destroy all resources)
```
make destroy
```

To destroy a specific resource, use make destroy-target
```
make destroy-target
```

## Kubernetes

The Terraform deployment will generate the k8s cluster and deploy the api. However, you may find that you would like to interact with the cluster after deployment.

The kubeconfig will be generated after the terraform apply completes (make apply), but an easier way to set up your kubeconfig might be to use this command:
```
aws eks --region $AWS_REGION update-kubeconfig --name <cluster name from TF output>
```

After that one-liner is run, you can run kubectl to explore the cluster.

Example:
```
kubectl get all -n live
```

## Terraform Documentation  

### Providers

| Name | Version |
|------|---------|
| aws | n/a |
| kubernetes | n/a |

### Modules

| Name | Source | Version |
|------|--------|---------|
| eks | terraform-aws-modules/eks/aws |  |
| vpc | terraform-aws-modules/vpc/aws |  |

### Resources

| Name |
|------|
| [aws_availability_zones](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/availability_zones) |
| [aws_eks_cluster](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster) |
| [aws_eks_cluster_auth](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/eks_cluster_auth) |
| [aws_route53_record](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route53_record) |
| [aws_route53_zone](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/route53_zone) |
| [kubernetes_deployment](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/deployment) |
| [kubernetes_namespace](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) |
| [kubernetes_service](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service) |

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| application | This is the name of the application or product | `string` | n/a | yes |
| container\_image | The container image which should be deployed. | `string` | `"registry.gitlab.com/liatrio-excercise/interview-api:latest"` | no |
| deployment | This is the deployment being created | `string` | n/a | yes |
| dns\_zone | The dns zone (domain name) to attach the application to. | `string` | n/a | yes |
| environment | This is the environment for these resources | `string` | n/a | yes |
### Outputs

| Name | Description |
|------|-------------|
| app\_dns | This is the application route\_53 CNAME |
| app\_lb\_endpoint | This is the application http endpoint address |
| cluster\_endpoint | Endpoint for EKS control plane. |
| cluster\_security\_group\_id | Security group ids attached to the cluster control plane. |
| config\_map\_aws\_auth | A kubernetes configuration to authenticate to this EKS cluster. |
| kubectl\_config | kubectl config as generated by the module. |
